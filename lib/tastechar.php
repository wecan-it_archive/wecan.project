<?php namespace Wecan\Project;

class TasteChar
{
    protected $items = [];

    public function getForItem(array $arItem)
    {
        $ids = \ArrayHelper::getValue($arItem, 'PROPERTIES.TASTE_CHARS.VALUE', []);

        if (empty($this->items)) {
            $this->load();
        }

        return \ArrayHelper::only($this->items, $ids);
    }

    protected function load()
    {
        $this->items = \Helper::get_from_cache('taste_chars', 1800, function () {
            $arItems = [];
            $obItems = \CIBlockElement::GetList([], [
                'IBLOCK_ID' => TASTE_CHARS_IBLOCK_ID,
                'ACTIVE'    => 'Y'
            ], false, false,
                ['ID', 'NAME', 'PROPERTY_IMAGE']
            );
            while ($arItem = $obItems->Fetch()) {
                $image = \CFile::ResizeImageGet($arItem['PROPERTY_IMAGE_VALUE'], [
                        'width'  => 32,
                        'height' => 32
                    ])['src'] ?? null;
                $arItems[$arItem['ID']] = [
                    'NAME'  => $arItem['NAME'],
                    'IMAGE' => $image
                ];
            }

            return $arItems;
        });
    }
}