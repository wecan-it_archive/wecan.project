<?php

namespace Wecan\Project\CData;

use Bitrix\Iblock\SectionTable;
use Wecan\Project\Common;
use Wecan\Project\QueryTo1C;

class CatchMoment
{
    /**
     * 1. получим все товары "Лови момент" из 1С
     * 2. выделим их xml_id
     * 3. найдем товары на сайте по xml_id
     * 4. обновим товары
     * 5. найдем и уберем товары, у которых признак "Лови момент", но которые нет в 1С
     *
     */
    public static function actualizeCatchMoment()
    {
        $arCProducts = QueryTo1C::getCatchMomentProducts();
        $arXml = \ArrayHelper::getColumn($arCProducts, 'id');
        $arProducts = \ArrayHelper::index(Common::getArProducts(['ID', 'XML_ID'], ['XML_ID' => $arXml]), 'XML_ID');
        self::updateProducts($arProducts, $arCProducts);
        self::deactivateCatchMomentProducts($arXml);
    }

    private static function deactivateCatchMomentProducts(array $arXml)
    {
        if (empty($arXml))
            return;

        $arProducts = Common::getArProducts(['ID'], [
            '!XML_ID' => $arXml,
            'PROP_CATCH_MOMENT_VALUE' => 'Y'
        ]);

        $propVal = Common::getArPropValueId('PROP_CATCH_MOMENT', ['Y']);

        if (!$propVal['Y'])
            return;
        else
            foreach ($arProducts as $product) {
                \CIBlockElement::SetPropertyValuesEx(
                    $product['ID'],
                    PRODUCTS_IBLOCK_ID,
                    [
                        'PROP_CATCH_MOMENT' => false
                    ]
                );
            }
    }

    private static function updateProducts(array $arProducts, array $arCProducts)
    {
        $arSectionXml = array_unique(
            \Helper::array_flatten(
                array_column($arCProducts, 'sections')
            )
        );
        $arSections = \ArrayHelper::index(SectionTable::getList([
            'select' => [
                'ID',
                'XML_ID'
            ],
            'filter' => [
                'XML_ID' => $arSectionXml
            ]
        ])->fetchAll(), 'XML_ID');

        $propVal = Common::getArPropValueId('PROP_CATCH_MOMENT', ['Y']);

        foreach ($arCProducts as $cProduct){
            if ($product = $arProducts[$cProduct['id']]) {
                $sections = [];
                if (is_array($cProduct['sections'])) {
                    foreach ($cProduct['sections'] as  $section) {
                        if (isset($arSections[$section]))
                            $sections[] = $arSections[$section]['ID'];
                    }
                }

                \CIBlockElement::SetPropertyValuesEx(
                    $product['ID'],
                    PRODUCTS_IBLOCK_ID,
                    [
                        'PROP_CATCH_MOMENT' => ($propVal['Y'])?: "",
                        'PROP_CATCH_MOMENT_DATE_ACTIVE_FROM' => $cProduct['date_active_from'],
                        'PROP_CATCH_MOMENT_DATE_ACTIVE_TO' => $cProduct['date_active_to'],
                        'PROP_CATCH_MOMENT_PRICE' => $cProduct['price']?: 0,
                        'PROP_CATCH_MOMENT_QUANTITY' => $cProduct['quantity']?: 0,
                        'PROP_CATCH_MOMENT_SECTIONS' => $sections?: false,
                    ]
                );
            }
        }
    }
}