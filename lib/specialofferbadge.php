<?php namespace Wecan\Project;

class SpecialOfferBadge
{
    protected $badges = [];

    public function getForItem(array $arItem)
    {
        $badgesIds = \ArrayHelper::getValue($arItem, 'PROPERTIES.SPECIALOFFERS.VALUE', []);

        if (empty($this->badges)) {
            $this->load();
        }

        return \ArrayHelper::only($this->badges, $badgesIds);
    }

    protected function load()
    {
        $this->badges = \Helper::get_from_cache('badges', 1800, function () {
            $arBadges = [];
            $obBadges = \CIBlockElement::GetList([], [
                'IBLOCK_ID' => SPECIALOFFER_BADGES_IBLOCK_ID,
                'ACTIVE'    => 'Y'
            ], false, false,
                ['ID', 'NAME', 'CODE', 'PROPERTY_BACKGROUND_COLOR', 'PROPERTY_TEXT_COLOR']
            );
            while ($arBadge = $obBadges->Fetch()) {
                $arBadges[$arBadge['ID']] = [
                    'NAME'             => $arBadge['NAME'],
                    'CODE'             => $arBadge['CODE'],
                    'TEXT_COLOR'       => $arBadge['PROPERTY_TEXT_COLOR_VALUE'],
                    'BACKGROUND_COLOR' => $arBadge['PROPERTY_BACKGROUND_COLOR_VALUE']
                ];
            }

            return $arBadges;
        });
    }
}