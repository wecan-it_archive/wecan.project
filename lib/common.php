<?php

namespace Wecan\Project;

use Bitrix\Iblock\ElementTable;
use Bitrix\Iblock\SectionTable;
use Bitrix\Main\Loader;
use Bitrix\Main\Type\Date;

class Common
{
    const MODULE_NAME = 'wecan.project';
    const LOG_FILE_OPTION = 'LOG_DIR';

    /**
     * @param array $select
     * @param array $filter
     * @return array
     */
    public static function getArProducts(array $select = [], array $filter = []): array
    {
        Loader::includeModule('iblock');
        $old = false;
        $result = [];

        foreach ($filter as $key=>$fil) {
            if (strpos($key, 'PROP_') !== false) {
                $old = true;
                break;
            }
        }

        if ($old) {
            $ob = \CIBlockElement::GetList(
                [],
                $filter,
                false,
                false,
                $select
            );

            while ($res = $ob->Fetch())
                $result[] = $res;
        } else
            $result = ElementTable::getList([
                'select' => $select,
                'filter' => array_merge($filter, ['IBLOCK_ID' => PRODUCTS_IBLOCK_ID])
            ])->fetchAll();

        return $result;
    }

    public static function getArPropValueId(string $propId, array $value): array
    {
        $res = [];

        $ob = \CIBlockPropertyEnum::GetList(
            [],
            [
                'VALUE' => $value,
                'CODE' => $propId
            ]
        );

        while ($result = $ob->Fetch()) {
            $res[$result['VALUE']] = $result['ID'];
        }

        return $res;
    }

    public static function getBitrixDate($date, string $format = 'DD-MM-YYYY') {
        return ($date)?
            new Date(\CDatabase::FormatDate($date, $format, 'DD.MM.YYYY'), 'd.m.Y') :
            null;
    }

    public static function writeLogs(string $title, array $logs = [])
    {
        $logFile = \COption::GetOptionString(self::MODULE_NAME, self::LOG_FILE_OPTION);
        if (!$logFile || empty($logs))
            return;

        $logFile = $_SERVER['DOCUMENT_ROOT'] . $logFile;

        if (file_exists($logFile)) {
            file_put_contents($logFile, "\n\n////////////////\n\n" . date("d-m-Y H-i") . ' ' . $title, FILE_APPEND);
            file_put_contents($logFile, "\nErrors:\n", FILE_APPEND);
            file_put_contents($logFile, implode("\n", $logs['error']), FILE_APPEND);
            file_put_contents($logFile, "\nMessage:\n", FILE_APPEND);
            file_put_contents($logFile, implode("\n", $logs['message']), FILE_APPEND);
        }
    }

    public function getSections(array $select, array $filter)
    {
        return SectionTable::getList([
            'select' => $select,
            'filter' => $filter
        ])->fetchAll();
    }

    public static function getBonus() {

        if (!Loader::includeModule('sale'))
            return false;

        global $USER;
        $uid = $USER->GetId();

        $userAccount = new \CSaleUserAccount;
        $accountByUser = $userAccount->GetByUserID($uid, \Wecan\Project\cdata\bonus::CURRENCY);
        $bonuses = round($accountByUser['CURRENT_BUDGET']);

        return $bonuses;
    }

    public static function getShortName($user) {
        $uid = $user->GetId();

        $rsUser = \CUser::GetList(($by="ID"), ($order="desc"), array("ID"=>$uid),array("SELECT"=>array("UF_*")));
        while ($arrUser = $rsUser->Fetch())
        {
            if ($arrUser['UF_NAME_LAST_NAME'] != null) {
                $names = explode(" ", $arrUser['UF_NAME_LAST_NAME']);
                if (count($names) >= 2) {
                    $n = mb_substr($names[0], 0, 1) . mb_substr($names[1], 0, 1);
                } else {
                    $n = mb_substr($names[0], 0, 1);
                }
            } else {
                $name = $user->GetFirstName();
                $surname = $user->GetLastName();

                if ($name != '' && $surname != '') {
                    $n = mb_substr($name, 0, 1) . mb_substr($surname, 0, 1);
                } elseif ($name != '' || $surname != '') {
                    if ($name != '') {
                        $n = mb_substr($name, 0, 1);
                    } else {
                        $n = mb_substr($surname, 0, 1);
                    }
                } else {
                    $login = $user->GetLogin();
                    $n = mb_substr($login, 0, 1);
                }
            }

            return $n;
        }
    }
}