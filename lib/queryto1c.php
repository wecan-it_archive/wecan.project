<?php
/**
 * Created by PhpStorm.
 * User: fans
 * Date: 10.01.17
 * Time: 11:36
 */

namespace Wecan\Project;


use Bitrix\Main\Loader;

class QueryTo1C
{
    private static $bonusQuery = '?action=getpromo&id=';
    private static $usersByPhoneQuery = '?action=getusers_byphone&phone=';
    private static $createCounterAgentQuery = '?action=newuser&id=';
    private static $createNewDialBackQuery = '?action=newDialBack&id=';
    private static $getCodeWordQuery = "?action=getCodeWordById&id=";

    private static function makeRequest(string $query): string
    {
        $client = new \GuzzleHttp\Client([
            'base_uri' => \COption::GetOptionString('wecan.project', '1C_URL')
        ]);

        try {
            $res = $client->request(
                'GET',
                $query,
                [
                    'auth' => [
                        \COption::GetOptionString('wecan.project', '1C_HTTP_USER'),
                        \COption::GetOptionString('wecan.project', '1C_HTTP_PASS')
                    ],
                    'headers' => [
                        'Content-Type' => 'application/json'
                    ]
                ]
            );

            if ($res->getStatusCode() != 200)
                Common::writeLogs(
                    'Query to 1C',
                    ['error' => ['Плохой статус ' . \COption::GetOptionString('wecan.project', '1C_URL') . $query . ": " . $res->getStatusCode()]]
                );
            else
                return $res->getBody();
        } catch (\Exception $e) {
            Common::writeLogs(
                'Query to 1C',
                ['error' => ['Не удалось произвести запрос по адресу ' . \COption::GetOptionString('wecan.project', '1C_URL') . $query . ": " . $e->getMessage()]]
            );
        }

        return '';
    }

    private static function parseResponse($res, $query): array
    {
        try {
            $arr = \GuzzleHttp\json_decode($res, true);

            return $arr;
        } catch (\Exception $e) {
            Common::writeLogs(
                'Query to 1C',
                [
                    'error' => ['Ошибка при разборе ответа по запросу ' . $query]
                ]
            );

            return [];
        }
    }

    public static function getBonuses(string $userXmlId): float
    {
        try {
            $res = self::makeRequest(self::$bonusQuery . $userXmlId);
            $arRes = self::parseResponse($res,self::$bonusQuery . $userXmlId);

            if ($arRes && $arRes['data'] && $arRes['data'][0]) {
                $arBonus = $arRes['data'][0];
                if (!isset($arBonus['id']) && !isset($arBonus['bonus']))
                    Common::writeLogs(
                        'Запрос бонусов для пользователя',
                        [
                            'error' => ['Нет необходимых полей в ответе для пользователя ' . $userXmlId]
                        ]
                    );
                else {
                    $userId = $arBonus['id'];
                    if ($userId != $userXmlId)
                        Common::writeLogs(
                            'Запрос бонусов для пользователя',
                            [
                                'error' => ['Не совпадают ид пользователя в запросе (' . $userXmlId . ') и ответе (' . $userId . ')']
                            ]
                        );
                    else
                        return $arBonus['bonus'];
                }
            } else {
                Common::writeLogs(
                    'Запрос бонусов для пользователя',
                    [
                        'error' => ['Нет данных для польхователя ' . $userXmlId]
                    ]
                );
            }
        } catch (\Exception $e) {
            Common::writeLogs(
                'Запрос бонусов для пользователя',
                [
                    'error' => ['Неудача для пользователя ' . $userXmlId . ':' . $e->getMessage()]
                ]
            );
        }

        return 0;
    }

    /**
     * Check one-use promo code in 1C
     *
     * @param string $promoCode
     * @return bool
     */
    public static function checkPromoCode(string $promoCode): bool
    {
        return true;
    }

    /**
     * Get users from 1C by Phone
     */
/*    public static function getUsersByPhone(string $phone): array
    {
        return [];
    }*/

    /**
     * get "catch moment" product info
     */
    public static function getCatchMomentProducts()
    {
        $json = '{
          "id": 177,
          "date_active_from": "01.02.2017",
          "date_active_to": "10.02.2017",
          "price": 200,
          "quantity": 10,
          "sections": [
              19,
              21
          ]
        }';

        $data = json_decode($json, true);
        if (json_last_error()) {
            Common::writeLogs('Parse CatchMomentData', ['error' => 'Ошибка пр разборе json: ' . json_last_error_msg()]);
            return [];
        }

        if (!isset($data['id']) || !$data['id'] || !isset($data['price']) || !$data['price']) {
            Common::writeLogs('Parse CatchMomentData', ['error' => 'Нет обязательных полей']);
            return false;
        } else
            return $data;
    }

    /**
     *
     * @param string $userXmlId
     * @return string
     */
    public static function getCodeWord(string $userXmlId): string
    {
        /* $result = '{
          "id": 177,
          "code": "wejkasjdf"
        }';*/

        $query = self::$getCodeWordQuery . $userXmlId;
        $response = self::makeRequest($query);
        $arRes = self::parseResponse($response, $query);

        if ($arRes) {
            if (!isset($arRes['id']) || !isset($arRes['code'])) {
                Common::writeLogs('Getting Code Word', ['error' => ['Required data is abent for user ' . $userXmlId]]);
                return "";
            }

            if ($arRes['id'] !== $userXmlId) {
                Common::writeLogs('Getting Code Word', ['error' => ['id in response don\'t equal to ' . $userXmlId]]);
                return "";
            }

            return $arRes['code'];

        } else {
            Common::writeLogs('Getting Code Word', ['error' => ['Empty response for user ' . $userXmlId]]);
            return "";
        }
    }

    /**
     * $result = '[{
     *  "id": 177,
     *   "name": "Василий",
     *   "last_name": "Бузова"
     *   },{
     *   "id": 17,
     *   "name": "Иван",
     *   "last_name": "Бородин"
     *  }]';
     *
     *
     * phone format: 9099099090
     *
     * @param int $phoneNumber
     * @return array
     */
    public static function getFioByPhone(int $phoneNumber, string $key): array
    {
        if ($_SESSION[$key])
            return $_SESSION[$key];

        //$response = self::makeRequest(self::$usersByPhoneQuery . $phoneNumber);
        $response = '';

        $arRes = self::parseResponse($response, self::$usersByPhoneQuery . $phoneNumber);
        $res = [];

        if ($arRes) {

            if ($arRes['response'] && $arRes['response'] == 'fail') {
                Common::writeLogs('Getting FIO by Phone ' . $phoneNumber, ['error' => [$arRes['error']?: 'fail']]);
                return [];
            }

            $res = array_filter($arRes, function ($ar) {
                if ((isset($ar['id']) && $ar['id']) &&
                    (
                        (isset($ar['name']) && $ar['name']) ||
                        (isset($ar['last_name']) && $ar['last_name'])
                    )
                )
                    return true;
                else {
                    Common::writeLogs('Getting FIO by Phone', ['error' => 'Required fields are absent']);
                    return false;
                }
            });

            if (!empty($res))
                $res = array_map(function ($i, $v) {
                    $fi = '';
                    if ($i['name'])
                        $fi = $i['name'];
                    if ($i['last_name']) {
                        if ($fi)
                            $fi .= ' ' . $i['last_name'];
                        else
                            $fi = $i['last_name'];
                    }

                    return $fi;
                }, $res);

        }

        return $res;
    }

    /**
     * $arProducts = [
     *      [
     *          'id' => 'productXmlId',
     *          'quantity' => 'quantityInBasket'
     *      ],
     *      [
     *          'id' => 'anotherProductXmlId',
     *          'quantity' => 'quantityInBasket'
     *      ],
     * ]
     *
     * @param string $districtXmlId
     * @param array $arProducts
     * @return string
     */
    public static function getTimeForDelivery(string $districtXmlId, array $arProducts): string
    {
        $stringFor1c = json_encode([
            'district' => $districtXmlId,
            'products' => $arProducts
        ]);
        // getDeliveryTime=$stringFor1c
        $result = '12:30'; // in 24 format
        return $result;
    }

    private static function parseJson(string $data, string $title, string $message)
    {
        $json = json_decode($data, true);
        if (json_last_error()) {
            Common::writeLogs($title, ['error' => $message . ': ' . json_last_error_msg()]);
            return false;
        }

        return $json;
    }

    /**
     * info from dialBack form
     * $arData = [
     *  'id' => 'siteId',
     *  'phone' => '9099099990',
     *  'date' => '23.01.2017 12:30'
     * ]
     *
     * @param array $arData
     * @return string
     */
    public static function sendNewDialBack(array $arData): string
    {
        $json = \GuzzleHttp\json_encode($arData);

        if (!$arData['id'] && !$arData['phone'] && !$arData['date']) {
            Common::writeLogs(
                'Sending new dialBack',
                [
                    'error' => ['неправильный массив: ' . $json]
                ]
            );

            return "";
        }

        $query = self::$createNewDialBackQuery . $json;
        $response = self::makeRequest($query);
        $arRes = self::parseResponse($response, $query);

        if ($arRes) {
            if ($arRes['response'] && $arRes['response'] == 'fail') {
                Common::writeLogs(
                    'Sending new dialBack',
                    [
                        'error' => ['query: ' . ($arRes['error']?: 'fail')]
                    ]
                );

                return "";
            } else {
                if ($id = $arRes['id'])
                    return $id;
                else
                    Common::writeLogs(
                        'Sending new dialBack',
                        [
                            'error' => ['query ' . $query . ': ' . "нет ид в ответе"]
                        ]
                    );
            }
        }

        return "";
    }

    /**
     * info from reservation form
     * $arData = [
     *  'id' => 'siteId',
     *  'data' => '23.01.2017',
     *  'time' => '12:30',
     *  'guest_count' => 4,
     *  'name' => 'Вася',
     *  'phone' => '9094567890'
     * ]
     *
     * @param array $arData
     */
    public static function sendNewReservation(array $arData)
    {
        // newReservation=json_endcode($arData)
        return "1C_code";
    }

    public static function sendParseResultLog(array $arLog)
    {
        //sendLog=json_encode($arLog)
    }

    /**
     * $userData = {
     *  "name" => "Name",
     *  "last_name" => "LastName",
     *  "email" => "email@email.com",
     *  "phone" => "9525555678",
     *  "birthday" => "23.05.1998",
     *  "id" => "ID"
     * }
     *
     * @param array $userData
     * @return string
     */
    public static function createCounterAgent(array $userData): string
    {
        $query = self::$createCounterAgentQuery . \GuzzleHttp\json_encode($userData);
        $response = self::makeRequest($query);
        $arRes = self::parseResponse($response, $query);

        return "";
        /* waiting answer from 1C programmer */
        if ($arRes) {
            if ($arRes['response'] && $arRes['response'] == 'fail') {
                Common::writeLogs(
                    'Creating counter-agent',
                    [
                        'error' => ['query: ' . ($arRes['error']?: 'fail')]
                    ]
                );

                return "";
            } else {
                //dump($arRes);
            }

        } else {
        }

        return "";
    }
}